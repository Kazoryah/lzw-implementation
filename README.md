# LZW Implementation

ING1 - CODO Project

by Kazoryah \<romain.hermary@epita.fr>


# Project Guidelines

## Compression
Compression must be launched with `$ python3 romain.hermary_LZW.py -c -p
path/filename.txt`

* `-c` Is the argument that ask for a compression
* `-p` Is the argument to specify the path of the text file

The program will write as an output:
* `filename_dico.csv` contains the dictionnary used by the compressor (in lexical order)
The reserved character is `%`, and cannot be used in the alphabet. The
dictionnary only contains the characters used by the compressor and the reserved
one. The last character of a file, python will terminate it by a return
character (`'\n'`), this symbol is not to be consider.
* `filename.lzw` contains the characters corresponding to the binary code written by
the compressor on the first line, the size before and after compression on the
second and third lines, and the compression ratio on the forth line.
* `filename_LZWtable.csv` contains the LZW compression table as if the compression
was done by hand. The first line is always the names of the columns.

The separator for the csv files is the comma. The files to write are written
in the current directory.

## Uncompression
Uncompression must be launched with `$ python3 romain.hermary_LZW.py -u -p
path/filename.lzw`

* `-u` Is the argument that ask for a uncompression
* `-p` As in the compression, but the `.lzw` file only contains the first line
of the compressor original output. The dictionnary file, under the name of
`filename_dico.csv` must be in the same directory as the compressed file.

The program will write as an output:
* `filename.txt` containing the decompressed text and written in the current
directory
