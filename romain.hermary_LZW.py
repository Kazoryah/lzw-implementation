from argparse import ArgumentParser
from argparse import RawTextHelpFormatter
import numpy as np
import csv

# The character used to denote a read of one more bit
special_char = '%'

# Parses the arguments passed to the program
# Throws an error if '-c' and '-u' are found in the same command line
def parse_arguments():
    message = "LZW Compression"

    parser = ArgumentParser(description=message,
                            formatter_class=RawTextHelpFormatter)

    parser.add_argument("-p", help="Path to the file",
                        metavar="PATH", action="store")
    parser.add_argument("-c", help="Compression",
                        action="store_true", default=False)
    parser.add_argument("-u", help="Uncompression",
                        action="store_true", default=False)

    arguments = parser.parse_args()
    assert arguments.c != arguments.u, "Choose between compression and uncrompression"

    return arguments

# Find the dictionnary for the input text
# Goes through all the characters in the file and add them in the dictionnary if
# they are not already in, sorts the resulting dictionnary in lexicographical
# order after adding the reserved character to it
# An error when the special character is found in the input file could be raised
def derive_dico(input_content, filename):
    dico = []

    for char in input_content:
        if not char in dico:
            dico.append(char)

    dico = [special_char] + dico
    dico.sort()

    file = open(filename + "_dico.csv", "w")
    csv_file = csv.writer(file, delimiter=',')

    csv_file.writerow(dico)

    file.close()

    return dico

# Writes a line in CSV format in the output table file
def write_table(csv_file, dico, buffer, input, new_sequence, address=None, output=None):
    if address == None:
        address = str(dico.index(new_sequence))

    if output == None:
        output = "@[" + buffer + "]=" + str(dico.index(buffer))

    csv_file.writerow([buffer, input, new_sequence, address, output])

# Compression function
# Derives the dictionnary, writes in the needed output files, read on precise
# number of bites... In short, follows the LZW compression mechanics and the
# subject constraints without much tricks
def compression(input_content, filename):
    dico = derive_dico(input_content, filename)

    if len(dico) == 1:
        return

    table_file = open(filename + "_LZWtable.csv", "w")
    table = csv.writer(table_file, delimiter=',')
    table.writerow(["Buffer", "Input", "New sequence", "Address", "Output"])

    table.writerow(['', input_content[0], '', '', ''])
    buffer = input_content[0]

    current_read = int(np.ceil(np.log2(len(dico))))
    max_address = 2 ** current_read - 1
    binary_format = "#0" + str(current_read + 2) + 'b'

    old_size = current_read * len(input_content)

    res = ''
    for input in input_content[1:]:
        new_sequence = buffer + input
        if new_sequence not in dico:
            dico.append(new_sequence)
            write_table(table, dico, buffer, input, new_sequence)

            res += format(dico.index(buffer), binary_format)[2:]
            buffer = input
        else:
            output = ''
            while dico.index(new_sequence) > max_address:
                res += format(dico.index(special_char), binary_format)[2:]
                output = "@[%]=" + str(dico.index(special_char))
                current_read += 1
                max_address = 2 ** current_read - 1
                binary_format = "#0" + str(current_read + 2) + 'b'

            write_table(table, dico, buffer, input, '', '', output)

            buffer += input

    write_table(table, dico, buffer, '', '', '')
    res += format(dico.index(buffer), binary_format)[2:]

    table_file.close()

    res_file = open(filename + ".lzw", "w")
    res_file.write(res)
    res_file.write("\nSize before LZW compression: " + str(old_size) + " bits\n")
    res_file.write("Size after LZW compression: " + str(len(res)) + " bits\n")
    res_file.write("Compression ratio: " + format(len(res) / old_size, ".3f"))
    res_file.close()

# Uncompression function
# Charges the dictionnary and the binary written file, write on the output file,
# follows the LZW uncompression mechanics
def uncompression(input_content, path, filename):
    dico_file = open(path[:-4] + "_dico.csv", "r")
    dico = list(csv.reader(dico_file, delimiter=','))[0]
    if len(dico) == 1:
        return

    current_read = int(np.ceil(np.log2(len(dico))))
    res = ''

    buffer = dico[int(input_content[0:current_read], 2)]

    index = current_read
    while index < len(input_content):
        address = int(input_content[index:index + current_read], 2)
        sequence = dico[address]

        if sequence == special_char:
            index += current_read
            current_read += 1
            continue

        new_sequence = buffer + sequence[0]
        if new_sequence not in dico:
            dico.append(new_sequence)

        res += buffer
        buffer = sequence
        index += current_read

    res_file = open(filename + ".txt", "w")
    res_file.write(res + buffer + '\n')
    res_file.close()

# Main passage
# Calls the functions following the arguments
# Cancels everything if the input file does not contain anything
if __name__ == "__main__":
    try:
        arguments = parse_arguments()
        filename = arguments.p.split("/")[-1][:-4]
        input_file = open(arguments.p, "r")
        input_content = input_file.read()

        if len(input_content):
            if input_content[-1] == '\n':
                input_content = input_content[:-1]

            if arguments.c:
                compression(input_content, filename)
            else:
                uncompression(input_content, arguments.p, filename)

        input_file.close()

    except AssertionError as error:
        print(error)
